import sys
import os
import filecmp
# getting the name of the directory
# where the this file is present.
current = os.path.dirname(os.path.realpath(__file__))
  
# Getting the parent directory name
# where the current directory is present.
parent = os.path.dirname(current)
  
# adding the parent directory to 
# the sys.path.
sys.path.append(parent)

from gilded_rose import GildedRose, Item, main


def test_default_item():
    default_item = Item("default", sell_in=10, quality=20)
    items = [default_item]
    shop = GildedRose(items)

    shop.update_quality()

    assert default_item.sell_in == 9
    assert default_item.quality == 19

def test_gm():
    sys.stdout = open('test-output.txt', 'w')
    main()
    sys.stdout.close()
    expected = open("../golden-master/expected-output.txt").read()
    test = open("test-output.txt").read()

    assert expected == test

def test_conjured():
    conjured_item = Item("Conjured", sell_in=-1, quality=20)
    items = [conjured_item]
    shop = GildedRose(items)
    shop.update_quality()
    assert conjured_item.sell_in == -2
    assert conjured_item.quality == 16
    shop.update_quality()
    assert conjured_item.sell_in == -3
    assert conjured_item.quality == 12
