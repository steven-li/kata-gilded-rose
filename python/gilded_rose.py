class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return f"Item(name={self.name} sell_in={self.sell_in}, quality={self.quality})"

    def __str__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)


def main():
    items = [
        Item(name="+5 Dexterity Vest", sell_in=10, quality=20),
        Item(name="Aged Brie", sell_in=2, quality=0),
        Item(name="Elixir of the Mongoose", sell_in=5, quality=7),
        Item(name="Sulfuras, Hand of Ragnaros", sell_in=0, quality=80),
        Item(name="Sulfuras, Hand of Ragnaros", sell_in=-1, quality=80),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=10, quality=49),
        Item(name="Backstage passes to a TAFKAL80ETC concert", sell_in=5, quality=49),
        Item(name="Conjured Mana Cake", sell_in=3, quality=6),
    ]

    gilded_rose = GildedRose(items)

    days = 30
    for day in range(days + 1):
        print("-------- day %s --------" % day)
        print("name, sellIn, quality")
        for item in items:
            print(item)
        print("")
        gilded_rose.update_quality()


class GildedRose(object):
    def __init__(self, items):
        self.items = items

    # /!\ Do not change code above this line /!\ #

    def update_quality(self):
        for item in self.items:
            if item.name != "Sulfuras, Hand of Ragnaros":
                item.sell_in = item.sell_in - 1
            if item.name == "Aged Brie":
                self.update_aged_brie(item)
            elif item.name == "Backstage passes to a TAFKAL80ETC concert":
                self.update_backstage(item)
            elif item.name == "Conjured":
                self.update_conjured(item)
            elif item.name != "Sulfuras, Hand of Ragnaros":
                if item.quality > 0:
                    item.quality = item.quality - 1
                    if item.sell_in < 0 and item.quality > 0:
                        item.quality = item.quality - 1
        
    def update_aged_brie(self, item):
        item.quality = item.quality + 1
        if item.sell_in < 0 :
            item.quality = item.quality + 1
        if item.quality > 50:
              item.quality = 50

    def update_backstage(self, item):
        if item.quality < 50:
            item.quality = item.quality + 1
            if item.sell_in < 10:
                if item.quality < 50:
                    item.quality = item.quality + 1
            if item.sell_in < 5:
                if item.quality < 50:
                    item.quality = item.quality + 1
        if item.sell_in < 0:
            item.quality = 0

    def update_conjured(self, item):
        if item.quality > 0:
            item.quality = item.quality - 2
        if item.sell_in < 0: 
            item.quality = item.quality - 2
        if item.quality < 0:
            item.quality = 0



if __name__ == "__main__":
    main()
